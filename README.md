## Full-Stack Project Skeleton (Spring Boot + Angular)
How to start:

1. download sources from the 'Downloads' section and extract them

2. open terminal, go to project's root and initialize repo with 'git init'

3. add the new prepared remote origin with 'git remote add origin ${remote.repo.url}'

4. initialize latest Angular project with 'ng new frontend' (angular-cli must be installed)

5. update 'start' script in frontend/package.json to look like "ng serve --proxy-config proxyconfig.json"

6. update root pom's artifactId to match your new project

7. build project with 'mvn clean package'

8. run project with 'java -jar backend/target/${parent.artifactId}-${parent.version}.jar'

9. open browser and go to http://localhost:8080

**Attention!** After first successful build to use the local instances of node, yarn and node_modules open terminal, go to frontend module root and run 'source .pathrc'.
Then you can use aliases 'build' and 'serve' to deal with frontend.
